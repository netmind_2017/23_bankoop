import java.util.Scanner;

public class menu {
	
	public static choise PrintMenu(Scanner scanner){		
		
		choise returnValue =choise.DEPOSIT; 
		
		System.out.println("Here are your menu options:");
		System.out.println("1. Make a deposit.");
		System.out.println("2. Make a withdrawal.");
		System.out.println("3. Print your balance.");
		System.out.println("4. Quit.");
		System.out.println("Please enter the number of your choice.");
		
		int val = scanner.nextInt();
		
		if (val ==1){
			returnValue = choise.DEPOSIT;
		}else if (val == 2) {
			returnValue = choise.WITHDRAW;
		}else if (val == 3) {
			returnValue = choise.BALANCE;
		}else if (val == 4) {
			returnValue = choise.EXIT;
		}		
		
		return returnValue;
	}
	
}
