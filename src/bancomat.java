
public class bancomat {
		
		private int balance;
		
		//CONSTRUCTOR
		public bancomat (int money){			
			this.balance = money;
		}
		
		public void PutMoney(int money) {
			this.balance = this.balance + money;
		}
		
		public void Withdraw(int money) { 
			this.balance = this.balance - money;
		}
		
		public int GetBalance() { 
			return this.balance;
		}		
		
}
